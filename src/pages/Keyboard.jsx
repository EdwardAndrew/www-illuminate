import { useState } from "react";
import Keyboard from "react-simple-keyboard";
import "react-simple-keyboard/build/css/index.css";
/** @jsx jsx */
// eslint-disable-next-line
import { jsx, css } from "@emotion/core";
import styled from "@emotion/styled";
import { CUEtoSimpleKeyboardMapping } from "../utils/CUEtoSimpleKeyboardMapping";
import { getReadableColor } from "../utils/utils";

export const IlluminateKeyboard = ({ conditions, triggers, colors }) => {
  const [mainKeyboardRef, setMainKeyboardRef] = useState();
  const [controlPadKeyboardRef, setControlPadKeyboardRef] = useState();
  const [arrowKeyboardRef, setArrowKeyboardRef] = useState();
  const [numPadKeyboardRef, setNumPadKeyboardRef] = useState();
  const [numPadEndKeyboardRef, setNumpadEndKeyboardRef] = useState();

  const [activeConditions, setActiveConditions] = useState([]);

  const conditionActive = condition => {
    const triggersToActivate = triggers.filter(key => {
      const result = key.conditions.every(
        c => condition.name === c || activeConditions.find(ac => ac.name === c)
      );
      return result;
    });
    triggersToActivate.forEach(trigger => {
      const skbValue = CUEtoSimpleKeyboardMapping[trigger.key];
      const button = getButtonElement(skbValue);
      if (!skbValue || !button) {
        console.error(`Failed to find button ${trigger.key}`);
        return;
      }
      const color = colors.find(col => col.name === trigger.color);
      button.style.backgroundColor = `rgb(${color.r},${color.g},${color.b})`;
      button.style.color = getReadableColor(color);
    });
  };

  const toggleCondition = condition => {
    const newConditions = activeConditions.filter(c => c.id !== condition.id);
    if (newConditions.length === activeConditions.length) {
      setActiveConditions([...activeConditions, condition]);
      conditionActive(condition);
    } else {
      setActiveConditions([...newConditions]);
    }
  };

  const [searchTerm, setSearchTerm] = useState("");
  const commonKeyboardOptions = {
    theme: "simple-keyboard hg-theme-default hg-layout-default",
    physicalKeyboardHighlight: false,
    syncInstanceInputs: true,
    mergeDisplay: true
  };

  const getButtonElement = key => {
    let result;
    const keyboardRefs = [
      mainKeyboardRef,
      controlPadKeyboardRef,
      arrowKeyboardRef,
      numPadKeyboardRef,
      numPadEndKeyboardRef
    ];

    keyboardRefs.forEach(keyboardRef => {
      if (!result) {
        result = keyboardRef.getButtonElement(key);
      }
    });
    return result;
  };

  const keyboardOptions = {
    ...commonKeyboardOptions,
    layout: {
      default: [
        "{escape} {f1} {f2} {f3} {f4} {f5} {f6} {f7} {f8} {f9} {f10} {f11} {f12}",
        "` 1 2 3 4 5 6 7 8 9 0 - = {backspace}",
        "{tab} q w e r t y u i o p [ ] \\",
        "{capslock} a s d f g h j k l ; ' {enter}",
        "{shiftleft} z x c v b n m , . / {shiftright}",
        "{controlleft} {altleft} {metaleft} {space} {metaright} {altright}"
      ]
    },
    display: {
      "{escape}": "esc ⎋",
      "{tab}": "tab ⇥",
      "{backspace}": "backspace ⌫",
      "{enter}": "enter ↵",
      "{capslock}": "caps lock ⇪",
      "{shiftleft}": "shift ⇧",
      "{shiftright}": "shift ⇧",
      "{controlleft}": "ctrl ⌃",
      "{controlright}": "ctrl ⌃",
      "{altleft}": "alt ⌥",
      "{altright}": "alt ⌥",
      "{metaleft}": "cmd ⌘",
      "{metaright}": "cmd ⌘"
    }
  };

  const keyboardControlPadOptions = {
    ...commonKeyboardOptions,
    layout: {
      default: [
        "{prtscr} {scrolllock} {pause}",
        "{insert} {home} {pageup}",
        "{delete} {end} {pagedown}"
      ]
    }
  };

  const keyboardArrowsOptions = {
    ...commonKeyboardOptions,
    layout: {
      default: ["{arrowup}", "{arrowleft} {arrowdown} {arrowright}"]
    }
  };

  const keyboardNumPadOptions = {
    ...commonKeyboardOptions,
    layout: {
      default: [
        "{numlock} {numpaddivide} {numpadmultiply}",
        "{numpad7} {numpad8} {numpad9}",
        "{numpad4} {numpad5} {numpad6}",
        "{numpad1} {numpad2} {numpad3}",
        "{numpad0} {numpaddecimal}"
      ]
    }
  };

  const keyboardNumPadEndOptions = {
    ...commonKeyboardOptions,
    layout: {
      default: ["{numpadsubtract}", "{numpadadd}", "{numpadenter}"]
    }
  };

  const ConditionsContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
  `;

  const Condition = styled.div`
    display: block;
    background-color: rgba(230, 230, 230);
    border-radius: 4px;
    margin: 5px;
    padding: 10px;
    user-select: none;
  `;

  return (
    <div>
      <h3 className="title is-3">Test your setup</h3>
      <div className="keyboardContainer">
        <Keyboard
          baseClass={"simple-keyboard-main"}
          keyboardRef={r => setMainKeyboardRef(r)}
          {...keyboardOptions}
        />
        <div className={"controlArrows"}>
          <Keyboard
            baseClass={"simple-keyboard-control"}
            keyboardRef={r => setControlPadKeyboardRef(r)}
            {...keyboardControlPadOptions}
          />
          <Keyboard
            baseClass={"simple-keyboard-arrows"}
            keyboardRef={r => setArrowKeyboardRef(r)}
            {...keyboardArrowsOptions}
          />
        </div>
        <div className={"numPad"}>
          <Keyboard
            baseClass={"simple-keyboard-numpad"}
            keyboardRef={r => setNumPadKeyboardRef(r)}
            {...keyboardNumPadOptions}
          />
          <Keyboard
            baseClass={"simple-keyboard-numpadEnd"}
            keyboardRef={r => setNumpadEndKeyboardRef(r)}
            {...keyboardNumPadEndOptions}
          />
        </div>
      </div>
      <div css={{ width: "500px", margin: "auto" }}>
        <label>
          Filter Conditions
          <input
            className="input"
            type="text"
            value={searchTerm}
            placeholder="Filter conditions by name"
            onChange={e => setSearchTerm(e.target.value)}
            css={{ width: "500px" }}
          />
        </label>
      </div>
      <ConditionsContainer>
        {conditions
          .filter(
            c => c.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
          )
          .map(condition => (
            <label className="checkbox">
              <Condition key={condition.id}>
                <input
                  type="checkbox"
                  onChange={() => toggleCondition(condition)}
                  checked={activeConditions.includes(condition)}
                />
                {condition.name}
              </Condition>
            </label>
          ))}
      </ConditionsContainer>
    </div>
  );
};
