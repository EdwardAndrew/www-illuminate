import React from "react";
import { Color } from "../components/Color";
import { Table, TableRow, TableHeading, AddButton } from "../components/elements";

export const Colors = ({
  value,
  onChange,
  deleteColor,
  addColorAtIndex,
  swapColor
}) => {
  return (
    <>
      <h3 className="title is-3">Choose the colors you like</h3>
      <Table>
        <thead>
          <TableRow>
            <TableHeading>
              <AddButton
                className="button"
                onClick={() => addColorAtIndex(0)}
              >
                <i className="fas fa-plus"></i>
              </AddButton>
            </TableHeading>
            <TableHeading>Name</TableHeading>
            <TableHeading>Color</TableHeading>
            <TableHeading />
            <TableHeading />
            <TableHeading />
          </TableRow>
        </thead>
        <tbody>
          {value.map((color, idx) => {
            return (
              <Color
                value={color}
                onChange={onChange}
                deleteColor={deleteColor(color)}
                key={color.id}
                index={idx}
                swapColor={swapColor}
              />
            );
          })}
        </tbody>
      </Table>
    </>
  );
};
