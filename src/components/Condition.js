import React, { useState } from "react";
import { TableCell, TableRow } from "./elements";

export const Condition = ({
  value: condition,
  deleteCondition,
  swapCondition,
  onChange,
  index
}) => {
  const [editMode, setEditMode] = useState(false);

  const onDrop = e => {
    e.preventDefault();
    const otherConditionIndex = e.dataTransfer.getData("conditionIdx");
    swapCondition(index, otherConditionIndex);
  };

  const onDragOver = e => {
    e.preventDefault();
  };

  const onDragStart = e => {
    e.dataTransfer.setData("conditionIdx", index);
  };

  const onValueChange = key => e => {
    onChange({ ...condition, [`${key}`]: e.target.value });
  };

  return (
    <TableRow>
      <TableCell
        style={{ cursor: "grabbing" }}
        onDrop={onDrop}
        onDragStart={onDragStart}
        draggable={true}
        onDragOver={onDragOver}
      >
        <i className="fas fa-sort"></i>
      </TableCell>
      <TableCell>
        <input
          className="input"
          value={condition.name}
          disabled={!editMode}
          onChange={onValueChange("name")}
        />
      </TableCell>
      <TableCell>
        {editMode ? (
          <button className="button is-info" onClick={() => setEditMode(false)}>
            Close
          </button>
        ) : (
          <button
            className="button is-info is-light"
            onClick={() => setEditMode(true)}
          >
            Edit
          </button>
        )}
      </TableCell>
      <TableCell>
        <input
          style={{ width: "400px" }}
          className="input"
          type="text"
          value={condition.dataRef}
          onChange={onValueChange("dataRef")}
          disabled={!editMode}
        />
      </TableCell>
      <TableCell>
        <div className="select">
          <select
            disabled={!editMode}
            onChange={onValueChange("match")}
            value={condition.match}
          >
            <option value="exactly">exactly</option>
            <option value="less_than">less_than</option>
            <option value="greater_than">greater_than</option>
          </select>
        </div>
      </TableCell>
      <TableCell>
        <input
          style={{ width: "50px" }}
          className="input"
          type="text"
          value={condition.index}
          disabled={!editMode}
          onChange={onValueChange("index")}
        />
      </TableCell>
      <TableCell>
        <input
          style={{ width: "50px" }}
          className="input"
          type="text"
          value={condition.value}
          disabled={!editMode}
          onChange={onValueChange("value")}
        />
      </TableCell>
      <TableCell>
        <button
          className="button is-danger is-light"
          onClick={() => {
            console.log("click");
            deleteCondition(condition);
          }}
          disabled={!editMode}
        >
          <i className="fas fa-minus-circle"></i>
        </button>
      </TableCell>
    </TableRow>
  );
};
