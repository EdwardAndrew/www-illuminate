import React, { useState } from "react";
import { CompactPicker } from "react-color";
import { TableRow, TableCell, ColorBlock } from "./elements";

export const Color = ({
  value,
  onChange,
  deleteColor,
  index,
  swapColor
}) => {
  const [color, setColor] = useState({ r: value.r, g: value.g, b: value.b });
  const [editMode, setEditMode] = useState(false);

  const onColorChange = e => {
    const { r, g, b } = e.rgb;
    setColor({ r, g, b });
    onChange({ ...value, r, g, b });
  };

  const onNameChange = e => {
    onChange({ ...value, ...color, name: e.target.value });
  };

  const onDrop = e => {
    e.preventDefault();
    const otherColorIndex = e.dataTransfer.getData("colorIdx");
    swapColor(index, otherColorIndex);
  };

  const onDragOver = e => {
    e.preventDefault();
  };

  const onDragStart = e => {
    e.dataTransfer.setData("colorIdx", index);
  };

  return (
    <TableRow>
      <TableCell
        style={{ cursor: "grabbing" }}
        onDrop={onDrop}
        onDragStart={onDragStart}
        draggable={true}
        onDragOver={onDragOver}
      >
        <i className="fas fa-sort"></i>
      </TableCell>
      <TableCell>
        <input
          className="input"
          type="text"
          value={value.name}
          onChange={onNameChange}
          disabled={!editMode}
        />
      </TableCell>
      <TableCell>
        <ColorBlock color={color} />
      </TableCell>
      <TableCell>
        {editMode ? (
          <button className="button is-info" onClick={() => setEditMode(false)}>
            Close
          </button>
        ) : (
          <button
            className="button is-info is-light"
            onClick={() => setEditMode(true)}
          >
            Edit
          </button>
        )}
      </TableCell>
      <TableCell>
        {editMode ? (
          <CompactPicker
            color={color}
            onChangeComplete={onColorChange}
            disabled={!editMode}
          />
        ) : null}
      </TableCell>
      <TableCell>
        <button
          className="button is-danger is-light"
          disabled={!editMode}
          onClick={deleteColor}
        >
          <i className="fas fa-minus-circle"></i>
        </button>
      </TableCell>
    </TableRow>
  );
};
