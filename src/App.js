/* eslint-disable */
import React, { useState, useEffect, useCallback } from "react";
import { capitalise } from "./utils/utils";
import { Colors } from "./pages/Colors";
import { Conditions } from "./pages/Conditions";
import { IlluminateKeyboard } from "./pages/Keyboard";
import { getRandomColor } from "./utils/utils";
import uuid from "uuid/v4";

import "./bulma.min.css";
import { Triggers } from "./pages/Triggers";
const dummyData = require("./illuminate.json");

const App = () => {
  let fileReader;
  const [colors, setColors] = useState([]);
  const [conditions, setConditions] = useState([]);
  const [keys, setKeys] = useState([]);
  const [activeTab, setActiveTab] = useState("test");

  useEffect(() => {
    updateStateFromJSON(dummyData);
  }, []);

  const addIdToObject = object => {
    if (!object.id) {
      object.id = uuid();
    }
    return object;
  };

  const updateStateFromJSON = json => {
    setColors(json.colors.map(addIdToObject));
    setConditions(json.conditions.map(addIdToObject));
    setKeys(json.keys.map(addIdToObject));
  };
  0;

  const fileHandler = file => {
    fileReader = new FileReader();
    fileReader.onloadend = e => {
      updateStateFromJSON(JSON.parse(fileReader.result));
    };
    fileReader.readAsText(file);
  };

  const switchNavigation = navigation => e => {
    e.preventDefault();
    setActiveTab(navigation);
  };

  const addColor = () => {
    const newColor = {
      id: uuid(),
      name: "New Color",
      ...getRandomColor()
    };
    setColors([newColor, ...colors]);
  };

  const onColorChange = color => {
    const index = colors.findIndex(c => c.id === color.id);
    const newColors = [...colors];

    const oldName = newColors[index].name;
    if (colors.findIndex(c => c.name === color.name) === -1) {
      newColors[index].name = color.name.trim();
    }
    newColors[index].r = color.r;
    newColors[index].g = color.g;
    newColors[index].b = color.b;

    const newKeys = [...keys];
    newKeys.forEach((k, i) => {
      if (k.color === oldName) {
        newKeys[i].color = newColors[index].name;
      }
    });

    setColors(newColors);
    setKeys(newKeys);
  };

  const addColorAtIndex = index => {
    const newColor = {
      id: uuid(),
      name: "New Color",
      ...getRandomColor()
    };

    const newColors = [...colors];
    newColors.splice(index, 0, newColor);
    setColors(newColors);
  };

  const swapColor = (aIndex, bIndex) => {
    const newColors = [...colors];
    const temp = newColors[aIndex];
    newColors[aIndex] = newColors[bIndex];
    newColors[bIndex] = temp;
    setColors(newColors);
  };

  const deleteColor = color => () => {
    setColors(colors.filter(c => c.id !== color.id));
  };

  const onConditionChange = condition => {
    const index = conditions.findIndex(c => c.id === condition.id);
    const newConditions = [...conditions];
    const oldName = newConditions[index].name;

    if (conditions.findIndex(c => c.name === condition.name) === -1) {
      newConditions[index].name = condition.name.trim();
    }
    newConditions[index].dataRef = condition.dataRef.trim();
    newConditions[index].index = condition.index;
    newConditions[index].match = condition.match;
    newConditions[index].value = condition.value;

    let newKeys = [...keys];
    newKeys.forEach((key, index) => {
      newKeys[index].conditions = key.conditions.map(c => {
        if (c === oldName) {
          return condition.name;
        }
        return c;
      });
    });
    setConditions(newConditions);
    setKeys(newKeys);
  };

  const addConditionAtIndex = index => {
    const newCondition = {
      id: uuid(),
      name: "New Condition",
      dataref: "",
      index: 0,
      match: "exactly",
      value: 1
    };

    const newConditions = [...conditions];
    newConditions.splice(index, 0, newCondition);
    setConditions(newConditions);
  };

  const deleteCondition = condition => {
    setConditions(conditions.filter(c => c.id !== condition.id));
  };

  const swapCondition = (aIndex, bIndex) => {
    const newConditions = [...conditions];
    const temp = newConditions[aIndex];
    newConditions[aIndex] = newConditions[bIndex];
    newConditions[bIndex] = temp;
    setConditions(newConditions);
  };

  const addTriggerAtIndex = index => {
    const newKey = {
      id: uuid(),
      key: 1,
      conditions: [],
      color: colors[0]
    };

    const newKeys = [...keys];
    newKeys.splice(index, 0, newKey);
    setKeys(newKeys);
  };

  const onTriggerChange = key => {
    const index = keys.findIndex(k => k.id === key.id);
    const newKeys = [...keys];
    newKeys[index].key = key.key;
    newKeys[index].color = key.color;
    newKeys[index].conditions = key.conditions;
    setKeys(newKeys);
  };

  const swapTrigger = (aIndex, bIndex) => {
    const newTriggers = [...keys];
    const temp = newTriggers[aIndex];
    newTriggers[aIndex] = newTriggers[bIndex];
    newTriggers[bIndex] = temp;
    setKeys(newTriggers);
  };

  const deleteTrigger = key => {
    setKeys(keys.filter(k => k.id !== key.id));
  };

  const navigation = [
    {
      fas: "fas fa-palette",
      text: "colors"
    },
    {
      fas: "fas fa-cog",
      text: "conditions"
    },
    {
      fas: "fas fa-magic",
      text: "triggers"
    },
    {
      fas: "fas fa-vial",
      text: "test"
    }
  ];

  const renderBody = () => {
    switch (activeTab) {
      case "profiles":
        return <div>Profiles</div>;
      case "aircraft":
        return <div>Aircraft</div>;
      case "colors":
        return (
          <Colors
            value={colors}
            onChange={onColorChange}
            deleteColor={deleteColor}
            addColor={addColor}
            swapColor={swapColor}
            addColorAtIndex={addColorAtIndex}
          />
        );
      case "conditions":
        return (
          <Conditions
            conditions={conditions}
            onChange={onConditionChange}
            deleteCondition={deleteCondition}
            swapCondition={swapCondition}
            addConditionAtIndex={addConditionAtIndex}
          />
        );
      case "triggers":
        return (
          <Triggers
            keys={keys}
            swapTrigger={swapTrigger}
            colors={colors}
            onChange={onTriggerChange}
            addTriggerAtIndex={addTriggerAtIndex}
            deleteTrigger={deleteTrigger}
          />
        );
      case "test":
        return <IlluminateKeyboard keys={keys} conditions={conditions} triggers={keys} colors={colors} />;
      default:
        return <div>Not found</div>;
    }
  };

  const save = () => {
    const fileData = JSON.stringify({ colors, conditions, keys });
    const blob = new Blob([fileData], { type: "application/json" });
    const url = URL.createObjectURL(blob);
    const link = document.createElement("a");
    link.download = "illuminate.json";
    link.href = url;
    link.click();
    link.remove();
  };

  return (
    <div>
      <header>
        <div className="hero">
          <div className="hero-body">
            <h1 className="title is-2">Illuminate</h1>
            <h5 className="subtitle is-5">
              RGB Keyboard Integration for X-Plane 11
            </h5>
            <span>
              Don't have it yet? Get it here!{" "}
              <a
                href="https://github.com/EdwardAndrew/XPlane-Illuminate/releases"
                target="_blank"
              >
                https://github.com/EdwardAndrew/XPlane-Illuminate/releases
              </a>
            </span>
          </div>
        </div>
      </header>
      <div className="level">
        <div className="level-item">
          <div className="level-item">
            <div className="file">
              <label className="file-label">
                <input
                  className={"file-input"}
                  type="file"
                  accept=".json"
                  onChange={e => fileHandler(e.target.files[0])}
                ></input>
                <span className="file-cta">
                  <span className="file-icon">
                    <i className="fas fa-upload"></i>
                  </span>
                  <span className="file-label">Choose a file…</span>
                </span>
              </label>
            </div>
          </div>
          <div className="level-item">
            {" "}
            <button className={"button"} onClick={save}>
              Save
            </button>
          </div>
        </div>
      </div>
      <div className="tabs">
        <ul>
          {navigation.map(navitem => (
            <li
              className={navitem.text === activeTab ? "is-active" : ""}
              key={`nav-${navitem.text}`}
            >
              <a onClick={switchNavigation(navitem.text)}>
                <span className="icon is-small">
                  <i className={navitem.fas}></i>
                </span>
                <span>{capitalise(navitem.text)}</span>
              </a>
            </li>
          ))}
        </ul>
      </div>
      <div className="section">{renderBody()}</div>
    </div>
  );
};
export default App;
