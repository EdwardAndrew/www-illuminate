import React from "react";
import { Trigger } from "../components/Trigger";
import {
  Table,
  TableRow,
  TableHeading,
  AddButton
} from "../components/elements";

export const Triggers = ({ keys, swapTrigger, colors, onChange, addTriggerAtIndex, deleteTrigger }) => {
  return (
    <>
      <h3 className="title is-3">Bind keys to conditions and colors</h3>
      <Table>
        <thead>
          <TableRow>
            <TableHeading>
              <AddButton className="button" onClick={() => addTriggerAtIndex(0)}>
                <i className="fas fa-plus"></i>
              </AddButton>
            </TableHeading>
            <TableHeading>Key to change</TableHeading>
            <TableHeading>When these conditions become true</TableHeading>
            <TableHeading>Set to color</TableHeading>
            <TableHeading></TableHeading>
          </TableRow>
        </thead>
        <tbody>
          {keys.map((key, idx) => {
            return (
              <Trigger
                value={key}
                key={key.id}
                swapTrigger={swapTrigger}
                index={idx}
                colors={colors}
                onChange={onChange}
                deleteTrigger={deleteTrigger}
              />
            );
          })}
        </tbody>
      </Table>
    </>
  );
};
