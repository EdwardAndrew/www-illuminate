import { KeyCodeCUEMappings } from './KeyCodeCUEMapping';
import { CUEKeys } from './CUE'

export const MakeColor = (name, value) => ({ name, value });

export const MakeKey = (conditions, key, color) => ({ conditions, key, color });

export const capitalise = string =>
  string === "" ? "" : `${string[0].toUpperCase()}${string.substring(1)}`;

export const calculateLuminance = rgb =>
  0.2126 * rgb.r + 0.7152 * rgb.g + 0.0722 * rgb.b;

export const getReadableColor = rgb =>
  calculateLuminance(rgb) < 140 ? "#fff" : "#000";

export const getRandomColor = () => ({
  r: Math.round(Math.random() * 255),
  g: Math.round(Math.random() * 255),
  b: Math.round(Math.random() * 255)
});

export const KeyCodeToCUE = keycode => KeyCodeCUEMappings.filter(o => o.key === keycode);

export const CUEToKeyCode = cue => KeyCodeCUEMappings.filter(o => o.cue === cue);

export const GetCUEByCode = CUECode => Object.entries(CUEKeys).find((e) => e[1] === CUECode);