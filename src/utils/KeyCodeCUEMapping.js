export const KeyCodeCUEMappings = [
  {
    cue: 1,
    key: 27
  },
  {
    cue: 2,
    key: 112
  },
  {
    cue: 3,
    key: 113
  },
  {
    cue: 4,
    key: 114
  },
  {
    cue: 5,
    key: 115
  },
  {
    cue: 6,
    key: 116
  },
  {
    cue: 7,
    key: 117
  },
  {
    cue: 8,
    key: 118
  },
  {
    cue: 9,
    key: 119
  },
  {
    cue: 10,
    key: 120
  },
  {
    cue: 11,
    key: 121
  },
  {
    cue: 12,
    key: 122
  },
  {
    cue: 13,
    key: 192
  },
  {
    cue: 14,
    key: 49
  },
  {
    cue: 15,
    key: 50
  },
  {
    cue: 16,
    key: 51
  },
  {
    cue: 17,
    key: 52
  },
  {
    cue: 18,
    key: 53
  },
  {
    cue: 19,
    key: 54
  },
  {
    cue: 20,
    key: 55
  },
  {
    cue: 21,
    key: 56
  },
  {
    cue: 22,
    key: 57
  },
  {
    cue: 23,
    key: 48
  },
  {
    cue: 24,
    key: 173
  },
  {
    cue: 25,
    key: 9
  },
  {
    cue: 26,
    key: 81
  },
  {
    cue: 27,
    key: 87
  },
  {
    cue: 28,
    key: 69
  },
  {
    cue: 29,
    key: 82
  },
  {
    cue: 30,
    key: 84
  },
  {
    cue: 31,
    key: 89
  },
  {
    cue: 32,
    key: 85
  },
  {
    cue: 33,
    key: 73
  },
  {
    cue: 34,
    key: 79
  },
  {
    cue: 35,
    key: 80
  },
  {
    cue: 36,
    key: 219
  },
  {
    cue: 37,
    key: 20
  },
  {
    cue: 38,
    key: 65
  },
  {
    cue: 39,
    key: 83
  },
  {
    cue: 40,
    key: 68
  },
  {
    cue: 41,
    key: 70
  },
  {
    cue: 42,
    key: 71
  },
  {
    cue: 43,
    key: 72
  },
  {
    cue: 44,
    key: 74
  },
  {
    cue: 45,
    key: 75
  },
  {
    cue: 46,
    key: 76
  },
  {
    cue: 47,
    key: 59
  },
  {
    cue: 48,
    key: 222
  },
  {
    cue: 49,
    key: 16
  },
  {
    cue: 50,
    key: 220
  },
  {
    cue: 51,
    key: 90
  },
  {
    cue: 52,
    key: 88
  },
  {
    cue: 53,
    key: 67
  },
  {
    cue: 54,
    key: 86
  },
  {
    cue: 55,
    key: 66
  },
  {
    cue: 56,
    key: 78
  },
  {
    cue: 57,
    key: 77
  },
  {
    cue: 58,
    key: 188
  },
  {
    cue: 59,
    key: 190
  },
  {
    cue: 60,
    key: 191
  },
  {
    cue: 61,
    key: 17
  },
  {
    cue: 62,
    key: 224
  },
  {
    cue: 63,
    key: 18
  },
  {
    cue: 64,
    key: null
  },
  {
    cue: 65,
    key: 32
  },
  {
    cue: 66,
    key: null
  },
  {
    cue: 67,
    key: null
  },
  {
    cue: 68,
    key: null
  },
  {
    cue: 69,
    key: 18
  },
  {
    cue: 70,
    key: null
  },
  {
    cue: 71,
    key: null
  },
  {
    cue: 72,
    key: null
  },
  {
    cue: 73,
    key: 123
  },
  {
    cue: 74,
    key: 44
  },
  {
    cue: 75,
    key: 145
  },
  {
    cue: 76,
    key: 19
  },
  {
    cue: 77,
    key: 45
  },
  {
    cue: 78,
    key: 36
  },
  {
    cue: 79,
    key: 33
  },
  {
    cue: 80,
    key: 221
  },
  {
    cue: 81,
    key: 220
  },
  {
    cue: 82,
    key: 223
  },
  {
    cue: 83,
    key: 13
  },
  {
    cue: 84,
    key: null
  },
  {
    cue: 85,
    key: 61
  },
  {
    cue: 86,
    key: null
  },
  {
    cue: 87,
    key: 8
  },
  {
    cue: 88,
    key: 46
  },
  {
    cue: 89,
    key: 35
  },
  {
    cue: 90,
    key: 34
  },
  {
    cue: 91,
    key: 16
  },
  {
    cue: 92,
    key: 17
  },
  {
    cue: 93,
    key: 38
  },
  {
    cue: 94,
    key: 37
  },
  {
    cue: 95,
    key: 40
  },
  {
    cue: 96,
    key: 39
  },
  {
    cue: 97,
    key: null
  },
  {
    cue: 98,
    key: null
  },
  {
    cue: 99,
    key: null
  },
  {
    cue: 100,
    key: null
  },
  {
    cue: 101,
    key: null
  },
  {
    cue: 102,
    key: null
  },
  {
    cue: 103,
    key: 144
  },
  {
    cue: 104,
    key: 111
  },
  {
    cue: 105,
    key: 106
  },
  {
    cue: 106,
    key: 109
  },
  {
    cue: 107,
    key: 107
  },
  {
    cue: 108,
    key: null
  },
  {
    cue: 109,
    key: 103
  },
  {
    cue: 110,
    key: 104
  },
  {
    cue: 111,
    key: 105
  },
  {
    cue: 112,
    key: null
  },
  {
    cue: 113,
    key: 100
  },
  {
    cue: 114,
    key: 101
  },
  {
    cue: 115,
    key: 102
  },
  {
    cue: 116,
    key: 97
  },
  {
    cue: 117,
    key: 98
  },
  {
    cue: 118,
    key: 99
  },
  {
    cue: 119,
    key: 96
  },
  {
    cue: 120,
    key: 108
  },
  {
    cue: 120,
    key: 110
  },
  {
    cue: 121,
    key: null
  },
  {
    cue: 122,
    key: null
  },
  {
    cue: 123,
    key: null
  },
  {
    cue: 124,
    key: null
  },
  {
    cue: 125,
    key: null
  },
  {
    cue: 126,
    key: null
  },
  {
    cue: 127,
    key: null
  },
  {
    cue: 128,
    key: null
  },
  {
    cue: 129,
    key: null
  },
  {
    cue: 130,
    key: null
  },
  {
    cue: 131,
    key: null
  },
  {
    cue: 132,
    key: null
  },
  {
    cue: 133,
    key: null
  },
  {
    cue: 134,
    key: null
  },
  {
    cue: 135,
    key: null
  },
  {
    cue: 136,
    key: null
  },
  {
    cue: 137,
    key: null
  },
  {
    cue: 138,
    key: null
  },
  {
    cue: 139,
    key: null
  },
  {
    cue: 140,
    key: null
  },
  {
    cue: 141,
    key: null
  },
  {
    cue: 142,
    key: null
  },
  {
    cue: 143,
    key: null
  },
  {
    cue: 144,
    key: null
  },
  {
    cue: 145,
    key: null
  },
  {
    cue: 146,
    key: null
  },
  {
    cue: 147,
    key: null
  },
  {
    cue: 148,
    key: null
  },
  {
    cue: 149,
    key: null
  },
  {
    cue: 150,
    key: null
  },
  {
    cue: 151,
    key: null
  },
  {
    cue: 152,
    key: null
  },
  {
    cue: 153,
    key: null
  },
  {
    cue: 154,
    key: null
  },
  {
    cue: 155,
    key: null
  }
];
