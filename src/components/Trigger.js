import React from "react";
import { TableRow, TableCell } from "./elements";
import { CUEKeys } from "../utils/CUE";
import { Tag } from "./Tag";

export const Trigger = ({
  value,
  swapTrigger,
  index,
  colors,
  onChange,
  deleteTrigger
}) => {
  const onDrop = e => {
    e.preventDefault();
    const otherTriggerIndex = e.dataTransfer.getData("triggerIdx");
    swapTrigger(index, otherTriggerIndex);
  };

  const onDragOver = e => {
    e.preventDefault();
  };

  const onDragStart = e => {
    e.dataTransfer.setData("triggerIdx", index);
  };

  const onKeyChange = e => {
    onChange({ ...value, key: parseInt(e.target.value) });
  };

  const onColorChange = e => {
    onChange({ ...value, color: e.target.value });
  };

  return (
    <TableRow>
      <TableCell
        style={{ cursor: "grabbing" }}
        onDrop={onDrop}
        onDragStart={onDragStart}
        draggable={true}
        onDragOver={onDragOver}
      >
        <i className="fas fa-sort"></i>
      </TableCell>
      <TableCell>
        <div className="select">
          <select value={value.key} onChange={onKeyChange}>
            {Object.entries(CUEKeys).map(([key, val]) => (
              <option value={val} key={val}>
                {key.replace("CLK_", "")}
              </option>
            ))}
          </select>
        </div>
      </TableCell>
      <TableCell>
        {value.conditions.map(condition => (
          <Tag key={`${value.id}-${condition}`}>{condition}</Tag>
        ))}
      </TableCell>
      <TableCell>
        <div className="select">
          <select value={value.color} onChange={onColorChange}>
            {colors.map(color => (
              <option value={color.name} key={color.id}>
                {color.name}
              </option>
            ))}
          </select>
        </div>
      </TableCell>
      <TableCell>
        <button
          className="button is-danger is-light"
          onClick={() => deleteTrigger(value)}
        >
          <i className="fas fa-minus-circle"></i>
        </button>
      </TableCell>
    </TableRow>
  );
};
