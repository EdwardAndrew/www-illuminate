/** @jsx jsx */
// eslint-disable-next-line
import { jsx, css } from "@emotion/core";
import styled from "@emotion/styled";

export const Table = styled.table`
  border: solid 2px rgba(230, 230, 230);
  margin: auto;
`;

export const TableCell = styled.td`
  padding: 10px;
  vertical-align: middle;
`;

export const TableRow = styled.tr`
  &:nth-child(even) {
    background-color: rgba(230, 230, 230);
  }
`;

export const TableHeading = styled.th`
  background-color: rgba(230, 230, 230);
  height: 40px;
  padding: 10px;
`;

export const AddButton = styled.button`
  width: 30px;
  height: 30px;
  vertical-align: middle;
  background-color: rgba(72, 199, 116, 0.5);
  color: #fff;
  &:hover {
    background-color: #48c774;
    color: #fff;
  }

  &:focus {
    background-color: #48c774;
    color: #fff;
  }
`;

export const ColorBlock = styled.div(({ color }) => ({
  backgroundColor: `rgb(${color.r}, ${color.g}, ${color.b})`,
  width: "40px",
  height: "40px",
  borderRadius: '4px'
}));
