/** @jsx jsx */
// eslint-disable-next-line
import { jsx, css } from "@emotion/core";
import styled from '@emotion/styled';


export const Tag = ({ children, onClick }) => {

    const style = css`
        font-size: 1em;
        background-color: #3298dc;
        border-radius: 4px;
        margin: 8px;
        padding: 8px;
        color: #fff;
        display: block;
    `;

    const IconWrapper = styled.div`
        cursor: pointer;
        float: right;
    `;

    const Text = styled.span`
    `;

    return (<div css={style}><Text>{children}</Text><IconWrapper onClick={onClick}><i className="fas fa-trash-alt"></i></IconWrapper></div>);
}
