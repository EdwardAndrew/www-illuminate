import React from "react";
import { Condition } from "../components/Condition";
import {
  TableHeading,
  TableRow,
  Table,
  AddButton
} from "../components/elements";

export const Conditions = ({
  conditions,
  onChange,
  deleteCondition,
  swapCondition,
  addConditionAtIndex
}) => {
  return (
    <>
      <div>
        <h3 className="title is-3">Create conditions based on datarefs</h3>
        <Table>
          <thead>
            <TableRow>
              <TableHeading>
                <AddButton
                  className="button"
                  onClick={() => addConditionAtIndex(0)}
                >
                  <i className="fas fa-plus"></i>
                </AddButton>
              </TableHeading>
              <TableHeading>Name</TableHeading>
              <TableHeading></TableHeading>
              <TableHeading>Data Ref</TableHeading>
              <TableHeading>Match</TableHeading>
              <TableHeading>Index*</TableHeading>
              <TableHeading>Value</TableHeading>
              <TableHeading />
            </TableRow>
          </thead>
          <tbody>
            {conditions.map((condition, idx) => (
              <Condition
                value={condition}
                key={condition.id}
                onChange={onChange}
                deleteCondition={deleteCondition}
                swapCondition={swapCondition}
                index={idx}
              />
            ))}
          </tbody>
        </Table>
      </div>
    </>
  );
};
